# Epic Compat: MrCrayfish's Gun Mod

MrCrayfish's Guns animations powered by Epic Fight.

## Description

Animations are only shown if Epic Fights animations are enabled. This is either the case if the player is in **battle mode** or if **filter animations** is **disabled** (in which case the mode doesn't matter). Beware that guns wielded in battle mode still do melee attacks on left click.

If you prefer vanilla 1st person animations in combination with Epic Fights animations in 3rd person you can use [Epic Tweaks](https://www.curseforge.com/minecraft/mc-mods/epic-tweaks) for that.

Also provides new weapon types that allow you to define custom items with the gun wielding animations. For more information see the [Epic Fight wiki](https://epicfight-docs.readthedocs.io/Guides/page2/#weapon-file). Note that any guns, from addons for Crayfishs gun mod, will automatically have an *Epic Fight weapon type* assigned to them if they are of any of the default *Crayfish gun type*.

| (Epic Fight) weapon type  | (Crayfish) gun type |
| ------------------------- | ------------------- |
| `epiccompat_cgm:pistol`   | `ONE_HANDED`        |
| `epiccompat_cgm:rifle`    | `TWO_HANDED`        |
| `epiccompat_cgm:bazooka`  | `BAZOOKA`           |
| `epiccompat_cgm:mini_gun` | `MINI_GUN`          |
| `epiccompat_cgm:grenade`  |                     |

## Dependencies

- [Epic Fight](https://www.curseforge.com/minecraft/mc-mods/epic-fight-mod)
- [MrCrayfish's Gun Mod](https://www.curseforge.com/minecraft/mc-mods/mrcrayfishs-gun-mod)
