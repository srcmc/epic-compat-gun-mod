# Changelog

## [1.0.1] - 2024-03-18

***Fixed***

- Incomplete mixin definition (causing crash on startup)

## [1.0.0] - 2024-03-18

***Added***

- Bazooka animations
- Grenade animations
- Mini Gun animations
- Pistol animations
- Rifle animations

***Fixed***

- Epic Fight/MrCrayfish's Gun Mod render incompatibility in smp
