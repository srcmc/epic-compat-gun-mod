/*
 * This file is part of Epic Compat: MrCrayfish's Gun Mod.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Epic Compat: MrCrayfish's Gun Mod is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Epic Compat: MrCrayfish's Gun Mod is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Epic Compat: MrCrayfish's Gun Mod. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.epiccompat_cgm.forge.gameasset.animation.types;

import com.mojang.math.Vector3f;

import net.minecraft.util.Mth;
import yesman.epicfight.api.animation.JointTransform;
import yesman.epicfight.api.animation.LivingMotions;
import yesman.epicfight.api.animation.Pose;
import yesman.epicfight.api.animation.types.AimAnimation;
import yesman.epicfight.api.animation.types.DynamicAnimation;
import yesman.epicfight.api.model.Armature;
import yesman.epicfight.api.utils.math.OpenMatrix4f;
import yesman.epicfight.world.capabilities.entitypatch.LivingEntityPatch;

/**
 * Fixes head jiggle glitch on sudden movement (e.g. while strafing or jumping).
 */
public class GunAimAnimation extends AimAnimation {
	public GunAimAnimation(boolean repeatPlay, String path1, String path2, String path3, String path4, Armature armature) {
		super(repeatPlay, path1, path2, path3, path4, armature);
	}
	
	@Override
	public void tick(LivingEntityPatch<?> entitypatch) {
		super.tick(entitypatch);
		
		var animator = entitypatch.getClientAnimator();
		var layer = animator.getCompositeLayer(this.getPriority());
		var player = layer.animationPlayer;
		
		if(isRepeat() && player.getElapsedTime() >= this.totalTime - 0.06F) {
			layer.resume(); // cancels pause()
		}
	}

	@Override
	public void modifyPose(DynamicAnimation animation, Pose pose, LivingEntityPatch<?> entitypatch, float time, float partialTicks) {
		if (!entitypatch.isFirstPerson()) {
			var chest = pose.getOrDefaultTransform("Chest");
			var head = pose.getOrDefaultTransform("Head");
			var f = 90.0F;
			var ratio = (f - Math.abs(entitypatch.getOriginal().getXRot())) / f;
			var yawOffset = entitypatch.getOriginal().yBodyRot;
			var yHeadRot = entitypatch.getOriginal().getYHeadRot();
			var qHead = Vector3f.YP.rotationDegrees(Mth.wrapDegrees(yawOffset - yHeadRot) * ratio);
			var qBody = Vector3f.YP.rotationDegrees(Mth.wrapDegrees(yHeadRot - yawOffset) * ratio);

			// forced head rotation for swim (crawl) motion
			if(entitypatch.currentLivingMotion == LivingMotions.SWIM /* || entitypatch.currentLivingMotion == LivingMotions.FLY */) {
				qHead.mul(Vector3f.XP.rotationDegrees(-80));
			}
			
			head.frontResult(JointTransform.getRotation(qHead), OpenMatrix4f::mulAsOriginFront);
			chest.frontResult(JointTransform.getRotation(qBody), OpenMatrix4f::mulAsOriginFront);
		}
	}
}
