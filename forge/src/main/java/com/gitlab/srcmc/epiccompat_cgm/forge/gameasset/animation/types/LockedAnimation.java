/*
 * This file is part of Epic Compat: MrCrayfish's Gun Mod.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Epic Compat: MrCrayfish's Gun Mod is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Epic Compat: MrCrayfish's Gun Mod is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Epic Compat: MrCrayfish's Gun Mod. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.epiccompat_cgm.forge.gameasset.animation.types;

import com.mojang.math.Vector3f;

import net.minecraft.util.Mth;
import yesman.epicfight.api.animation.JointTransform;
import yesman.epicfight.api.animation.Pose;
import yesman.epicfight.api.animation.types.DynamicAnimation;
import yesman.epicfight.api.animation.types.StaticAnimation;
import yesman.epicfight.api.model.Armature;
import yesman.epicfight.api.utils.math.OpenMatrix4f;
import yesman.epicfight.world.capabilities.entitypatch.LivingEntityPatch;

/**
 * Locks head rotation according to animation regardless of camera rotation.
 */
public class LockedAnimation extends StaticAnimation {
    public LockedAnimation(boolean repeatPlay, String path, Armature armature) {
        super(repeatPlay, path, armature);
    }

	@Override
	public void modifyPose(DynamicAnimation animation, Pose pose, LivingEntityPatch<?> entitypatch, float time, float partialTicks) {
        super.modifyPose(animation, pose, entitypatch, time, partialTicks);

		if (!entitypatch.isFirstPerson()) {
			var head = pose.getOrDefaultTransform("Head");
			var yawOffset = entitypatch.getOriginal().yBodyRot;
			var yHeadRot = entitypatch.getOriginal().getYHeadRot();
			head.frontResult(JointTransform.getRotation(Vector3f.YP.rotationDegrees(Mth.wrapDegrees(yawOffset - yHeadRot))), OpenMatrix4f::mulAsOriginFront);
			// TODO: prevent head from rotating with camera on x-Axis
		}
	}
}
