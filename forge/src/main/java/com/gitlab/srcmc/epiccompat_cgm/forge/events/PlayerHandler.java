/*
 * This file is part of Epic Compat: MrCrayfish's Gun Mod.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Epic Compat: MrCrayfish's Gun Mod is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Epic Compat: MrCrayfish's Gun Mod is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Epic Compat: MrCrayfish's Gun Mod. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.epiccompat_cgm.forge.events;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.gitlab.srcmc.epiccompat_cgm.ModCommon;
import com.mrcrayfish.guns.init.ModSyncedDataKeys;
import com.mrcrayfish.guns.item.GunItem;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import yesman.epicfight.api.animation.LivingMotion;
import yesman.epicfight.api.animation.LivingMotions;
import yesman.epicfight.api.client.forgeevent.UpdatePlayerMotionEvent;

@Mod.EventBusSubscriber(modid = ModCommon.MOD_ID, bus = Bus.FORGE, value = Dist.CLIENT)
public class PlayerHandler {
    static Map<UUID, LivingMotion> preLivingMotions = new HashMap<>();

    @SubscribeEvent
    static void onLogout(PlayerEvent.PlayerLoggedOutEvent event) {
        preLivingMotions.remove(event.getEntity().getUUID());
    }

    @SubscribeEvent
    static void onPlayerMotionComposite(UpdatePlayerMotionEvent.BaseLayer event) {
        var playerPatch = event.getPlayerPatch();
        var player = playerPatch.getOriginal();

        if(player.getMainHandItem().getItem() instanceof GunItem) {
            if(ModSyncedDataKeys.AIMING.getValue(player)) {
                preLivingMotions.put(player.getUUID(), playerPatch.currentLivingMotion);
                playerPatch.currentLivingMotion = LivingMotions.AIM;
            }
        }
    }

    @SubscribeEvent
    static void onPlayerMotionComposite(UpdatePlayerMotionEvent.CompositeLayer event) {
        var playerPatch = event.getPlayerPatch();
        var player = playerPatch.getOriginal();
        var preLivingMotion = preLivingMotions.get(player.getUUID());

        if(preLivingMotion != null) {
            playerPatch.currentLivingMotion = preLivingMotion;
            preLivingMotions.remove(player.getUUID());
        }

        if(player.getMainHandItem().getItem() instanceof GunItem) {
            if(ModSyncedDataKeys.RELOADING.getValue(player)) {
                playerPatch.currentCompositeMotion = LivingMotions.RELOAD;
            } else if(ModSyncedDataKeys.AIMING.getValue(player)) {
                playerPatch.currentCompositeMotion = LivingMotions.AIM;
            } else if(ModSyncedDataKeys.SHOOTING.getValue(player)) {
                playerPatch.getClientAnimator().playReboundAnimation();
            } else {
                playerPatch.currentCompositeMotion = playerPatch.currentLivingMotion;
            }
        }
    }
}
