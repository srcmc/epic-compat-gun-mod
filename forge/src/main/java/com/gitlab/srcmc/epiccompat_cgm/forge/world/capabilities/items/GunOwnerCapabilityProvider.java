/*
 * This file is part of Epic Compat: MrCrayfish's Gun Mod.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Epic Compat: MrCrayfish's Gun Mod is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Epic Compat: MrCrayfish's Gun Mod is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Epic Compat: MrCrayfish's Gun Mod. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.epiccompat_cgm.forge.world.capabilities.items;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.gitlab.srcmc.epiccompat_cgm.ModCommon;
import com.gitlab.srcmc.epiccompat_cgm.forge.ModCapabilities;
import com.mrcrayfish.guns.item.GunItem;

import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingTickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;

@EventBusSubscriber(modid = ModCommon.MOD_ID, bus = Bus.FORGE)
public class GunOwnerCapabilityProvider implements ICapabilityProvider {
    public static class OwnerId { public int value; }
    private LazyOptional<OwnerId> id = LazyOptional.of(() -> new OwnerId());

    @Override
    public <T> @NotNull LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side) {
       return cap == ModCapabilities.OWNER_ID ? id.cast() : LazyOptional.empty();
    }

    @SubscribeEvent
    static void onAttachCapabilitiesToItemStack(AttachCapabilitiesEvent<ItemStack> event) {
        if(event.getObject().getItem() instanceof GunItem) {
            event.addCapability(
                new ResourceLocation(ModCommon.MOD_ID, "owner_id"),
                new GunOwnerCapabilityProvider());
        }
    }

    @SubscribeEvent
    static void onLivingTick(LivingTickEvent event) {
        event.getEntity().getMainHandItem()
            .getCapability(ModCapabilities.OWNER_ID)
            .ifPresent(cap -> cap.value = event.getEntity().getId());
    }
}
