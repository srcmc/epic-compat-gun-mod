/*
 * This file is part of Epic Compat: MrCrayfish's Gun Mod.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Epic Compat: MrCrayfish's Gun Mod is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Epic Compat: MrCrayfish's Gun Mod is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Epic Compat: MrCrayfish's Gun Mod. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.epiccompat_cgm.forge.world.capabilities.items;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.mrcrayfish.guns.common.GripType;
import com.mrcrayfish.guns.item.GrenadeItem;
import com.mrcrayfish.guns.item.GunItem;

import net.minecraft.core.Direction;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.common.util.NonNullSupplier;
import yesman.epicfight.world.capabilities.EpicFightCapabilities;
import yesman.epicfight.world.capabilities.item.CapabilityItem;

public class GunCapabilityProvider implements ICapabilityProvider, NonNullSupplier<CapabilityItem> {
    private final LazyOptional<CapabilityItem> optional = LazyOptional.of(this);
    private CapabilityItem capability;

    public GunCapabilityProvider(ItemStack itemStack) {
        if(itemStack.getItem() instanceof GunItem gunItem) {
            var gripType = gunItem.getGun().getGeneral().getGripType();

            this.capability = gripType == GripType.ONE_HANDED
                ? GunCapabilityPresets.PISTOL.apply(gunItem).build() : gripType == GripType.TWO_HANDED
                ? GunCapabilityPresets.RIFLE.apply(gunItem).build() : gripType == GripType.BAZOOKA
                ? GunCapabilityPresets.BAZOOKA.apply(gunItem).build() : gripType == GripType.MINI_GUN
                ? GunCapabilityPresets.MINI_GUN.apply(gunItem).build() : null;
        } else if(itemStack.getItem() instanceof GrenadeItem grenadeItem) {
            this.capability = GunCapabilityPresets.GRENADE.apply(grenadeItem).build();
        }
    }

    @Override
    public <T> @NotNull LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side) {
        return cap == EpicFightCapabilities.CAPABILITY_ITEM ? this.optional.cast() : LazyOptional.empty();
    }

    @Override
    public @NotNull CapabilityItem get() {
        return this.capability;
    }

    public boolean hasCapability() {
        return this.capability != null;
    }
}
