/*
 * This file is part of Epic Compat: MrCrayfish's Gun Mod.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Epic Compat: MrCrayfish's Gun Mod is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Epic Compat: MrCrayfish's Gun Mod is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Epic Compat: MrCrayfish's Gun Mod. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.epiccompat_cgm.forge.gameasset.animation;

import com.gitlab.srcmc.epiccompat_cgm.ModCommon;
import com.gitlab.srcmc.epiccompat_cgm.forge.gameasset.animation.types.GunMoveAnimation;
import com.gitlab.srcmc.epiccompat_cgm.forge.gameasset.animation.types.LockedAnimation;
import com.gitlab.srcmc.epiccompat_cgm.forge.gameasset.animation.types.GunAimAnimation;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import yesman.epicfight.api.animation.types.ReboundAnimation;
import yesman.epicfight.api.animation.types.StaticAnimation;
import yesman.epicfight.api.forgeevent.AnimationRegistryEvent;
import yesman.epicfight.gameasset.Armatures;

@Mod.EventBusSubscriber(modid = ModCommon.MOD_ID, bus = Bus.MOD)
public class Animations {
	public static StaticAnimation BIPED_HOLD_PISTOL;
	public static StaticAnimation BIPED_WALK_PISTOL;
	public static StaticAnimation BIPED_RUN_PISTOL;
	public static StaticAnimation BIPED_SNEAK_PISTOL;
	public static StaticAnimation BIPED_PISTOL_AIM;
	public static StaticAnimation BIPED_PISTOL_RELOAD;

	public static StaticAnimation BIPED_HOLD_RIFLE;
	public static StaticAnimation BIPED_WALK_RIFLE;
	public static StaticAnimation BIPED_RUN_RIFLE;
	public static StaticAnimation BIPED_SNEAK_RIFLE;
	public static StaticAnimation BIPED_RIFLE_AIM;
	public static StaticAnimation BIPED_RIFLE_RELOAD;

	public static StaticAnimation BIPED_HOLD_BAZOOKA;
	public static StaticAnimation BIPED_WALK_BAZOOKA;
	public static StaticAnimation BIPED_RUN_BAZOOKA;
	public static StaticAnimation BIPED_SNEAK_BAZOOKA;
	public static StaticAnimation BIPED_BAZOOKA_AIM;
	public static StaticAnimation BIPED_BAZOOKA_RELOAD;

	public static StaticAnimation BIPED_HOLD_MINI_GUN;
	public static StaticAnimation BIPED_WALK_MINI_GUN;
	public static StaticAnimation BIPED_RUN_MINI_GUN;
	public static StaticAnimation BIPED_SNEAK_MINI_GUN;
	public static StaticAnimation BIPED_MINI_GUN_AIM;
	public static StaticAnimation BIPED_MINI_GUN_RELOAD;

	public static StaticAnimation BIPED_GRENADE_THROW;
	public static StaticAnimation BIPED_GRENADE_ARM;

	@SubscribeEvent
	static void registerAnimations(AnimationRegistryEvent event) {
		event.getRegistryMap().put(ModCommon.MOD_ID, Animations::build);
	}

    private static void build() {
        var biped = Armatures.BIPED;
        
		BIPED_HOLD_PISTOL = new GunAimAnimation(true, "biped/living/hold_pistol_mid", "biped/living/hold_pistol_up", "biped/living/hold_pistol_down", "biped/living/hold_pistol_up", biped);
        BIPED_WALK_PISTOL = new GunMoveAnimation(true, "biped/living/walk_pistol_mid", "biped/living/hold_pistol_up", "biped/living/hold_pistol_down", "biped/living/hold_pistol_up", biped);
        BIPED_RUN_PISTOL = new GunMoveAnimation(true, "biped/living/run_pistol_mid", "biped/living/hold_pistol_up", "biped/living/hold_pistol_down", "biped/living/hold_pistol_up", biped);
        BIPED_SNEAK_PISTOL = new GunMoveAnimation(true, "biped/living/sneak_pistol_mid", "biped/living/hold_pistol_up", "biped/living/hold_pistol_down", "biped/living/hold_pistol_up", biped);
		BIPED_PISTOL_AIM = new GunAimAnimation(true, "biped/combat/pistol_aim_mid", "biped/combat/pistol_aim_up", "biped/combat/pistol_aim_down", "biped/combat/pistol_aim_up", biped);
		BIPED_PISTOL_RELOAD = new LockedAnimation(true, "biped/combat/pistol_reload", biped);

		BIPED_HOLD_RIFLE = new GunAimAnimation(true, "biped/living/hold_rifle_mid", "biped/living/hold_rifle_up", "biped/living/hold_rifle_down", "biped/living/hold_rifle_up", biped);
        BIPED_WALK_RIFLE = new GunMoveAnimation(true, "biped/living/walk_rifle_mid", "biped/living/hold_rifle_up", "biped/living/hold_rifle_down", "biped/living/hold_rifle_up", biped);
        BIPED_RUN_RIFLE = new GunMoveAnimation(true, "biped/living/run_rifle_mid", "biped/living/hold_rifle_up", "biped/living/hold_rifle_down", "biped/living/hold_rifle_up", biped);
        BIPED_SNEAK_RIFLE = new GunMoveAnimation(true, "biped/living/sneak_rifle_mid", "biped/living/hold_rifle_up", "biped/living/hold_rifle_down", "biped/living/hold_rifle_up", biped);
		BIPED_RIFLE_AIM = new GunAimAnimation(true, "biped/combat/rifle_aim_mid", "biped/combat/rifle_aim_up", "biped/combat/rifle_aim_down", "biped/combat/rifle_aim_up", biped);
		BIPED_RIFLE_RELOAD = new LockedAnimation(true, "biped/combat/rifle_reload", biped);

		BIPED_HOLD_BAZOOKA = new GunAimAnimation(true, "biped/living/hold_bazooka_mid", "biped/living/hold_bazooka_up", "biped/living/hold_bazooka_down", "biped/living/hold_bazooka_up", biped);
        BIPED_WALK_BAZOOKA = new GunMoveAnimation(true, "biped/living/walk_bazooka_mid", "biped/living/hold_bazooka_up", "biped/living/hold_bazooka_down", "biped/living/hold_bazooka_up", biped);
        BIPED_RUN_BAZOOKA = new GunMoveAnimation(true, "biped/living/run_bazooka_mid", "biped/living/hold_bazooka_up", "biped/living/hold_bazooka_down", "biped/living/hold_bazooka_up", biped);
        BIPED_SNEAK_BAZOOKA = new GunMoveAnimation(true, "biped/living/sneak_bazooka_mid", "biped/living/hold_bazooka_up", "biped/living/hold_bazooka_down", "biped/living/hold_bazooka_up", biped);
		BIPED_BAZOOKA_AIM = new GunAimAnimation(true, "biped/combat/bazooka_aim_mid", "biped/combat/bazooka_aim_up", "biped/combat/bazooka_aim_down", "biped/combat/bazooka_aim_up", biped);
		BIPED_BAZOOKA_RELOAD = new LockedAnimation(true, "biped/combat/bazooka_reload", biped);

		BIPED_HOLD_MINI_GUN = new GunAimAnimation(true, "biped/living/hold_mini_gun_mid", "biped/living/hold_mini_gun_up", "biped/living/hold_mini_gun_down", "biped/living/hold_mini_gun_up", biped);
        BIPED_WALK_MINI_GUN = new GunMoveAnimation(true, "biped/living/walk_mini_gun_mid", "biped/living/hold_mini_gun_up", "biped/living/hold_mini_gun_down", "biped/living/hold_mini_gun_up", biped);
        BIPED_RUN_MINI_GUN = new GunMoveAnimation(true, "biped/living/run_mini_gun_mid", "biped/living/hold_mini_gun_up", "biped/living/hold_mini_gun_down", "biped/living/hold_mini_gun_up", biped);
        BIPED_SNEAK_MINI_GUN = new GunMoveAnimation(true, "biped/living/sneak_mini_gun_mid", "biped/living/hold_mini_gun_up", "biped/living/hold_mini_gun_down", "biped/living/hold_mini_gun_up", biped);
		BIPED_MINI_GUN_AIM = new GunAimAnimation(true, "biped/living/hold_mini_gun_mid", "biped/living/hold_mini_gun_up", "biped/living/hold_mini_gun_down", "biped/living/hold_mini_gun_up", biped);
		BIPED_MINI_GUN_RELOAD = new LockedAnimation(true, "biped/combat/mini_gun_reload", biped);

		BIPED_GRENADE_ARM = new GunAimAnimation(false, "biped/combat/grenade_arm", "biped/combat/grenade_arm", "biped/combat/grenade_arm", "biped/combat/grenade_arm", biped);

		// TODO: custom rebound animation (special handling while in swim (crawl) pose -> head rotation)
		BIPED_GRENADE_THROW = new ReboundAnimation(false, "biped/combat/grenade_throw", "biped/combat/grenade_throw", "biped/combat/grenade_throw", "biped/combat/grenade_throw", biped);
    }
}
