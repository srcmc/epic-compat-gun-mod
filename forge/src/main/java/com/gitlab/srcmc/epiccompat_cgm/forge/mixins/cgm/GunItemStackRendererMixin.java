/*
 * This file is part of Epic Compat: MrCrayfish's Gun Mod.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Epic Compat: MrCrayfish's Gun Mod is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Epic Compat: MrCrayfish's Gun Mod is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Epic Compat: MrCrayfish's Gun Mod. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.epiccompat_cgm.forge.mixins.cgm;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import com.gitlab.srcmc.epiccompat_cgm.forge.ModCapabilities;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mrcrayfish.guns.client.GunItemStackRenderer;
import com.mrcrayfish.guns.client.handler.GunRenderingHandler;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.renderer.BlockEntityWithoutLevelRenderer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.block.model.ItemTransforms;
import net.minecraft.client.renderer.block.model.ItemTransforms.TransformType;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderDispatcher;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;

@Mixin(GunItemStackRenderer.class)
public class GunItemStackRendererMixin extends BlockEntityWithoutLevelRenderer {
    public GunItemStackRendererMixin(BlockEntityRenderDispatcher berd, EntityModelSet ems) {
        super(berd, ems);
    }

    // EpicFight uses a custom rendering system which causes this method to be called
    // for players other than the local player. This is not expected by Crayfishs Gun
    // Mod which causes a visual bug when firing a gun (muzzles are drawn for ALL
    // players holding a gun). This overwrite provides a workaround to fix this issue.
    // A custom item capability is used to retrieve the actual owner of a gun.
    @Overwrite(remap = false)
    public void renderByItem(ItemStack stack, ItemTransforms.TransformType transform, PoseStack poseStack, MultiBufferSource source, int light, int overlay) {
        var mc = Minecraft.getInstance();
        var id = stack.getCapability(ModCapabilities.OWNER_ID).orElse(null);
        LivingEntity living = null;

        if(id != null) {
            if(mc.level.getEntity(id.value) instanceof LivingEntity entity) {
                living = entity;
            }
        }

        if(living == null) {
            living = mc.player;
        }

        poseStack.popPose();
        poseStack.pushPose();

        if(transform == TransformType.GROUND) {
            GunRenderingHandler.get().applyWeaponScale(stack, poseStack);
        }

        GunRenderingHandler.get().renderWeapon(living, stack, transform, poseStack, source, light, Minecraft.getInstance().getDeltaFrameTime());
        poseStack.popPose();
        poseStack.pushPose();
    }
}
