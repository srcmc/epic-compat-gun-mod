/*
 * This file is part of Epic Compat: MrCrayfish's Gun Mod.
 * Copyright (c) 2024, HDainester, All rights reserved.
 *
 * Epic Compat: MrCrayfish's Gun Mod is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Epic Compat: MrCrayfish's Gun Mod is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along
 * with Epic Compat: MrCrayfish's Gun Mod. If not, see <http://www.gnu.org/licenses/lgpl>.
 */
package com.gitlab.srcmc.epiccompat_cgm.forge.gameasset.animation.types;

import yesman.epicfight.api.model.Armature;
import yesman.epicfight.world.capabilities.entitypatch.LivingEntityPatch;

/**
 * GunAimAnimation that adjusts its play speed to player movement (see MovementAnimation).
 */
public class GunMoveAnimation extends GunAimAnimation {
	public GunMoveAnimation(boolean repeatPlay, String path1, String path2, String path3, String path4, Armature armature) {
		super(repeatPlay, path1, path2, path3, path4, armature);
	}

	@Override
	public float getPlaySpeed(LivingEntityPatch<?> entitypatch) {
		float movementSpeed = 1.0F;
		
		if (Math.abs(entitypatch.getOriginal().animationSpeed - entitypatch.getOriginal().animationSpeedOld) < 0.007F) {
			movementSpeed *= (entitypatch.getOriginal().animationSpeed * 1.16F);
		}
		
		return movementSpeed;
	}
	
	@Override
	public boolean canBePlayedReverse() {
		return true;
	}
}
